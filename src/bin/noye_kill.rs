extern crate noye_kill;

use noye_kill::*;

fn main() {
    let mut args = std::env::args();
    if args.len() < 2 {
        eprintln!("no process provided");
        std::process::exit(1);
    }

    let name = args.nth(1).expect("should be able to get arg");
    match find_process(&name) {
        Some(pid) => kill_process(pid),
        None => {
            eprintln!("could not get pid for: '{}'", name);
            std::process::exit(1);
        }
    };

    std::process::exit(0);
}
