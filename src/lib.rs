extern crate winapi;
#[macro_use]
extern crate log;

use std::process::Command;
use winapi::um::consoleapi;
use winapi::um::wincon;

pub type PID = u32;

pub fn find_process(name: &str) -> Option<PID> {
    let tasklist = Command::new("tasklist")
        .arg("/NH") // no header
        .arg("/FO") // output type
        .arg("CSV") // comma-separated values
        .arg("/FI") // filter
        .arg(&format!("IMAGENAME eq {}", name)) // filter to image name
        .output()
        .expect("expected to be able to run tasklist");

    if !tasklist.status.success() {
        return None;
    }

    let data = &tasklist.stdout;
    // shitty csv "parsing"
    let mut parts = data.split(|b| *b == b',');
    if let Some(pid) = parts.nth(1) {
        let s = String::from_utf8_lossy(pid);
        let s = s.replace('"', " ");
        s.trim().parse::<u32>().ok()
    } else {
        None
    }
}

pub fn kill_process(pid: PID) {
    info!("killing pid: {}", pid);
    unsafe {
        // remove our parent console
        wincon::FreeConsole();
        // non zero success
        if wincon::AttachConsole(pid) > 0 {
            // detach it from self
            consoleapi::SetConsoleCtrlHandler(None, 1);
            // send CTRL_BREAK_EVENT to process group
            wincon::GenerateConsoleCtrlEvent(wincon::CTRL_BREAK_EVENT, pid);
        }
    }
}
